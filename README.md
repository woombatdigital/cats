## Test ##

A json web service has been set up at the url: http://agl-developer-test.azurewebsites.net/people.json

You need to write some code to consume the json and output a list of all the cats in alphabetical order under a heading of the gender of their owner.

You can write it in any language you like. You can use any libraries/frameworks/SDKs you choose.

Example:

Male

* Angel
* Molly
* Tigger

Female

* Gizmo
* Jasper


## Answer ##

Source :

https://bitbucket.org/woombatdigital/cats/src

Live Example Here :

http://woombat.com/CATS/cats.html